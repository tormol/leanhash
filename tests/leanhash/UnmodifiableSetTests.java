/* Copyright 2016 Torbjørn Birch Moltu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package leanhash;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.Map.Entry;
import junit.framework.Test;
import junit.framework.TestSuite;
import com.google.common.collect.testing.CollectionTestSuiteBuilder;
import com.google.common.collect.testing.MapTestSuiteBuilder;
import com.google.common.collect.testing.MinimalCollection;
import com.google.common.collect.testing.SetTestSuiteBuilder;
import com.google.common.collect.testing.SortedSetTestSuiteBuilder;
import com.google.common.collect.testing.TestStringCollectionGenerator;
import com.google.common.collect.testing.TestStringMapGenerator;
import com.google.common.collect.testing.TestStringSetGenerator;
import com.google.common.collect.testing.TestStringSortedSetGenerator;
import com.google.common.collect.testing.features.*;

/**Can only test features from existing interfaces*/
public class UnmodifiableSetTests {
	public static Test suite() {
		TestSuite s = new TestSuite("tbm.util.collections");
		s.addTest(SetTestSuiteBuilder
				.using(new TestStringSetGenerator() {@Override protected Set<String> create(String[] elements) {
					return UnmodifiableSet.small(elements);}})
				.named("UnmodifiableSmallSet")
				.withFeatures(
						CollectionFeature.REJECTS_DUPLICATES_AT_CREATION,
						CollectionFeature.ALLOWS_NULL_VALUES,
						CollectionFeature.SERIALIZABLE,
						CollectionFeature.KNOWN_ORDER,
						CollectionSize.ANY)
				.createTestSuite());
		s.addTest(SortedSetTestSuiteBuilder
				.using(new TestStringSortedSetGenerator() {@Override protected SortedSet<String> create(String[] elements) {
					return new UnmodifiableSortedSet<String>(elements);}})
				.named("UnmodifiableSortedSet")
				.withFeatures(
						CollectionFeature.REJECTS_DUPLICATES_AT_CREATION,
						CollectionFeature.ALLOWS_NULL_QUERIES,
						CollectionFeature.SERIALIZABLE,
						CollectionSize.ANY)
				.createTestSuite());
		s.addTest(SetTestSuiteBuilder
				.using(new TestStringSetGenerator() {@Override protected Set<String> create(String[] elements) {
					return new UnmodifiableStartTableHashSet<String>(elements, false);}})
				.named("UnmodifiableStartTableHashSet")
				.withFeatures(
						CollectionFeature.REJECTS_DUPLICATES_AT_CREATION,
						CollectionFeature.ALLOWS_NULL_QUERIES,
						CollectionFeature.SERIALIZABLE,
						CollectionSize.ANY)
				.createTestSuite());
		return s;
	}
}
