/* Copyright 2016 Torbjørn Birch Moltu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package leanhash;
import java.util.Set;
import com.google.common.collect.testing.MinimalCollection;
import com.google.common.collect.testing.SetTestSuiteBuilder;
import com.google.common.collect.testing.TestStringSetGenerator;
import com.google.common.collect.testing.features.CollectionFeature;
import com.google.common.collect.testing.features.CollectionSize;
import com.google.common.collect.testing.features.SetFeature;
import junit.framework.Test;
import junit.framework.TestSuite;

public class SetWithGetTests {
	public static Test suite() {
		TestSuite s = new TestSuite("tbm.util.collections.SetWithGet");
		s.addTest(SetTestSuiteBuilder
				.using(new TestStringSetGenerator() {@Override protected Set<String> create(String[] elements) {
					return SetWithGet.from(new java.util.HashSet<String>(MinimalCollection.of(elements)));}})
				.named("WrapSetToAddGet")
				.withFeatures(
						SetFeature.GENERAL_PURPOSE,
						CollectionFeature.SERIALIZABLE,
						CollectionFeature.ALLOWS_NULL_VALUES,
						CollectionSize.ANY)
				.createTestSuite());
		return s;
	}

	//TODO test WrapSetToAddGet
	//TODO test ImmutableSet
	//TODO test LeanHashSet
}
