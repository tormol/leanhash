/* Copyright 2016 Torbjørn Birch Moltu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package leanhash;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import junit.framework.TestSuite;
import com.google.common.collect.testing.MapTestSuiteBuilder;
import com.google.common.collect.testing.MinimalCollection;
import com.google.common.collect.testing.SetTestSuiteBuilder;
import com.google.common.collect.testing.TestStringMapGenerator;
import com.google.common.collect.testing.TestStringSetGenerator;
import com.google.common.collect.testing.features.*;

/**Can only test features from existing interfaces*/
@RunWith(Suite.class)
@Suite.SuiteClasses({
	LeanHashTests.JUnit4Wrapper.class,
	LeanHashTests.JUnit3Wrapper.class
})public class LeanHashTests {

public static class JUnit3Wrapper {
	public static junit.framework.Test suite() {
		TestSuite s = new TestSuite("tbm.util.LeanHash");
		s.addTest(SetTestSuiteBuilder
				.using(new TestStringSetGenerator() {@Override protected Set<String> create(String[] elements) {
					return new LeanHashSet<String>(MinimalCollection.of(elements));}})
				.named("LeanHashSet")
				.withFeatures(
						SetFeature.GENERAL_PURPOSE,
						CollectionFeature.SERIALIZABLE,
						CollectionSize.ANY)
				.createTestSuite());
		s.addTest(MapTestSuiteBuilder
				.using(new TestStringMapGenerator() {@Override protected Map<String, String> create(Entry<String, String>[] entries) {
						Map<String,String> map = new LeanHashMap<String, String>();
						for (Entry<String,String> e : entries)
							map.put(e.getKey(), e.getValue());
						return map;
					}})
				.named("LeanHashMap")
				.withFeatures(
						MapFeature.GENERAL_PURPOSE,
						MapFeature.ALLOWS_NULL_VALUES,
						MapFeature.ALLOWS_NULL_ENTRY_QUERIES,
						CollectionFeature.REMOVE_OPERATIONS,
						CollectionFeature.SERIALIZABLE,
						CollectionSize.ANY)
				.createTestSuite());
		return s;
	}
}


public static class JUnit4Wrapper {
	//TODO test LeanHashSets SetWithGet implementation
	//TODO test LeanHashMap.iterator();
}
}
