# Leanhash

The main feature of this library is `LeanHashMap` and `LeanHashSet`,
which are memory-efficient alternatives to te `java.util` equivalents.
I'm not actually sure they're correct hash structures, but they use `hashCode()`
and work. To be compact, they don't store size anywhere,
so `.size()` and `.isEmpty()` is O(buckets).

Other collections and interfaces:
* `HasKey`/`SetWithGet`: For when the key is already a part of the value
* `ArrayCollection`: A raw collection / bag; If you don't care about order and want O(1) remove.
* `Immutable*Set`: Made for fun, there are probably better implementations elsewhere.

The classes that implement existing interfaces,
such as `Collection`, `Iterator`, `Set` and `Map`, passed the guava test suite
when they were written.  
The implementations of my interfaces aren't as thoroughly tested.

## License

Licensed under the Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)

### Contribution

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the work by you, as defined in the Apache-2.0
license, shall be licensed as above, without any additional terms or
conditions.
