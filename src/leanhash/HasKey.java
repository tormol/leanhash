/* Copyright 2016 Torbjørn Birch Moltu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package leanhash;
import java.util.Objects;

//TODO javadoc
/**When you want to use SetWithGet
 * This interface adds requirements to some Object methods:
 * 	hashCode() to be the same as getKey().hashCode()
 * 	equals() must only use key for comparison, and should accept Keys in adittion to it's own class
 * You should extend AbstractHasKey if possible.*/
public interface HasKey<K> {
	/**Should never change nor return {@code null}*/
	K getKey();



	/**implements hashCode() and equals() correctly*/
	public abstract class AbstractHasKey<K> implements HasKey<K> {
		/***/@Override//Object
		public final boolean equals(Object o) {
			if (o == this)
				return true;
			if (o == null)
				return false;
			if (o.getClass() == this.getClass())
				return getKey().equals(((AbstractHasKey<?>)o).getKey());
			if (getKey().getClass().isAssignableFrom(o.getClass()))
				return getKey().equals(o);
			return false;
		}
	
		/**@return {@code getKey().hashCode()}*/@Override//Object
		public final int hashCode() {
			return getKey().hashCode();
		}
	
		/**@return {@code getKey().toString()}*/@Override//Object
		public String toString() {
			return getKey().toString();
		}
	}



	public static class CompleteHasKey<K> extends AbstractHasKey<K> {
		protected final K key;
		protected CompleteHasKey(K key) {
			this.key = Objects.requireNonNull(key);
		}
		@Override public final K getKey() {
			return key;
		}
	}
}
