/* Copyright 2016 Torbjørn Birch Moltu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package leanhash;
import java.util.Arrays;

/**A set that support any element except null, and uses a hash function for contains()*/
public abstract class UnmodifiableHashSet<E> extends UnmodifiableSet<E> {
	/**0 <= hash < elements.length
	 *@param o not null*/
	protected abstract int hash(Object o);

	protected UnmodifiableHashSet(Object[] elements, boolean fromSet) {
		super(elements, !fromSet);
	}

	/**Check for duplicates by setting an element to ArrayCollection.empty and see if the set still contains() the removed element.
	 *Makes a copy of elements if it's not an Object[]
	 *@throws IllegalArgumentException if one elements equals another*/
	@Override protected void checkForDuplicates() throws IllegalArgumentException {
		Object[] elements = this.elements;
		if (elements.getClass() == Object[].class)
			elements = Arrays.copyOf(elements, elements.length, Object[].class);
		for (int i=0; i<elements.length; i++) {
			Object e = elements[i];
			elements[i] = ArrayCollection.empty;
			if (contains(e))
				throw new IllegalArgumentException("multiple "+e+'s');
			elements[i] = e;
		}
	}

	/**@return -1 if o is null or not equal()ed by elements[hash(o)] or a following index with the same hash*/
	@Override protected int indexOf(Object o) {
		if (o == null)
			return -1;
		int hash = hash(o);
		int index = hash;
		do {
			if (elements[index] == null)
				return -1;
			if (elements[index].equals(o))
				return index;
			index++;
		} while (index < elements.length  &&  hash(elements[index]) == hash);
		return -1;
	}

	private static final long serialVersionUID = 1L;
}
