/* Copyright 2016 Torbjørn Birch Moltu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package leanhash;
import java.util.Collection;
import java.lang.reflect.Array;

public interface CollectionWithToArrayType<E> extends Collection<E> {
	/**Get an array of a certain type without instantiating a zero-length array or calling size() twice
	 *@param ofType type of the desired array
	 * (It's easier to get component type of an array than the array type of a component).
	 */@SuppressWarnings("unchecked")
	default <T> T[] toArray(Class<T[]> ofType) {
		return toArray((T[])Array.newInstance(ofType.getComponentType(), 0));
	}

	@Override default Object[] toArray() {
		return toArray(Object[].class);
	}
}
