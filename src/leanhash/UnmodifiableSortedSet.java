/* Copyright 2016 Torbjørn Birch Moltu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package leanhash;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.SortedSet;

public class UnmodifiableSortedSet<E extends Comparable<E>> extends UnmodifiableSet<E> implements SortedSet<E> {
	/**Create an UnmodifiableSortedSet with these elements.*/@SafeVarargs//constructor accepts Object[]
	public static <E extends Comparable<E>> UnmodifiableSortedSet<E> with(E... elements) {
		return new UnmodifiableSortedSet<E>(elements);
	}

	protected UnmodifiableSortedSet(Object[] elements, boolean sorted, boolean fromSet) {
		super(elements, true);
		if ( !sorted)
			Arrays.sort(elements);
		if ( !fromSet)
			checkForDuplicates();
	}
	@Override protected int indexOf(Object o) {
		if (o == null)
			return -1;
		return Arrays.binarySearch(elements, o);
	}
	@Override protected void checkForDuplicates() {
		for (int i=1; i<elements.length; i++)
			if (Arrays.binarySearch(elements, 0, i, elements[i])  >=  0)
				throw new IllegalArgumentException("multiple "+elements[i]+'s');
	}


	public UnmodifiableSortedSet(E[] elements) throws IllegalArgumentException {
		this(elements, false, false);
	}
	public UnmodifiableSortedSet(Collection<? extends E> col) throws IllegalArgumentException {
		this(col.toArray(), false, false);
	}
	public UnmodifiableSortedSet(Set<? extends E> set) throws IllegalArgumentException {
		this(set.toArray(), false, true);
	}
	public UnmodifiableSortedSet(SortedSet<? extends E> set) {
		this(set.toArray(), true, true);
	}



	  /////////////////////
	 //SortedSet methods//
	/////////////////////

	/**{@inheritDoc}
	 *Always returns {@code null}*/@Override
	public Comparator<? super E> comparator() {
		return null;
	}

	@SuppressWarnings("unchecked")
	protected E elementAt(int index) {
		try {
			return (E)elements[index];
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new NoSuchElementException();
		}
	}
	@Override public E first() {
		return elementAt(0);
	}
	@Override public E last() {
		return elementAt(elements.length-1);
	}

	protected final int wouldBeIndexOf(Object o) {
		int startIndex = indexOf(o);
		if (startIndex < 0)
			startIndex = ~startIndex;//"clever"
		return startIndex;
	}
	protected UnmodifiableSortedSet<E> subSet(int from, int to) {//make public when needed.
		return new UnmodifiableSortedSet<E>(Arrays.copyOfRange(elements, from, to), true, true);
	}
	@Override public UnmodifiableSortedSet<E> subSet(E from, E to) {
		return subSet(wouldBeIndexOf(from), wouldBeIndexOf(to));
	}
	@Override public SortedSet<E> headSet(E endsWith) {
		return subSet(0, wouldBeIndexOf(endsWith));
	}
	@Override public SortedSet<E> tailSet(E startsWith) {
		return subSet(wouldBeIndexOf(startsWith), elements.length);
	}

	private static final long serialVersionUID = 1L;
}
